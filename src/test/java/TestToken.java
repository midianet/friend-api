import midianet.friend.domain.Token;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class TestToken {

    @Test
    public void testUsedKeys() {
        Token t = Token.getInstance();
        for (int i = 0; i < 1795; i++) {
            Assert.assertNotNull(t.getKey());
        }
    }
}