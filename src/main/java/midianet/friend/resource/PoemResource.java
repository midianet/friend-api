package midianet.friend.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.domain.Poem;
import midianet.friend.repository.PoemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/poems")
public class PoemResource {

    private List<Poem> poems;

    @Autowired
    private PoemRepository repository;

    private int current;

    @PostConstruct
    private void load() {
        poems = repository.findAll();
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Poem getPoem() {
        if (!ObjectUtils.isEmpty(poems)) {
            if (current >= poems.size()) current = 0;
            var p = poems.get(current);
            current++;
            return p;
        } else {
            throw new EntityNotFoundException();
        }
    }

}