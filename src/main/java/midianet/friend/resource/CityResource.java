package midianet.friend.resource;

//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.log4j.Log4j2;
import midianet.friend.api.tempo.Tempo;
import midianet.friend.domain.City;
import midianet.friend.domain.Token;
import midianet.friend.repository.CityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Log4j2
@RestController
@RequestMapping("/api/cities")
public class CityResource {

    @Autowired
    private CityRepository repository;

    private RestTemplate restTemplate = new RestTemplate();

    @Transactional
    @GetMapping(path="/refresh" )
    public void refresh(){
        var token = Token.getInstance().getKey();
        var ret = restTemplate.getForObject("http://apiadvisor.climatempo.com.br/api/v1/locale/city?token=".concat(token), City[].class);
        repository.deleteAll();
        Arrays.asList(ret).forEach(city -> {
            city.setName(Normalizer.normalize(city.getName(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "").toLowerCase());
            repository.save(city);
        });
    }

    @GetMapping
    public Tempo findByNameAndState(@RequestParam String name, @RequestParam(name = "state", required = false) String state) {
        List<City> list;
        if (ObjectUtils.isEmpty(state)) {
            list = repository.findByName(name.toLowerCase().replace("%20"," "));
        } else {
            list = repository.findByNameAndState(name.toLowerCase().replace("%20"," "), state.toUpperCase());
        }
        if (ObjectUtils.isEmpty(list)) throw new EmptyResultDataAccessException("Cidade não encontrada.", 1);
        if (list.size() > 1) throw new IncorrectResultSizeDataAccessException(list.size());
        return callApi(list.get(0).getId());
    }

    @HystrixCommand(fallbackMethod    = "fallbackCallClimaTempoApi",
                    commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "20000")})
    private Tempo callApi(Long cityId){
        var token = Token.getInstance().getKey();
        return restTemplate.getForObject(String.format("http://apiadvisor.climatempo.com.br/api/v1/weather/locale/%d/current?token=%s", cityId, token), Tempo.class);
    }

    @SuppressWarnings("unused")
    public Tempo fallbackCallClimaTempoApi(Long cityId) throws TimeoutException{
        throw new TimeoutException("Api Clima tempo fora");
    }

}