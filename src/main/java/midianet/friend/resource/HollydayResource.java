package midianet.friend.resource;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import midianet.friend.api.hollyday.Calendar;
import midianet.friend.domain.Hollyday;
import midianet.friend.repository.HollydayRepository;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/hollydays")
public class HollydayResource {

    @Autowired
    private HollydayRepository repository;

    @Transactional
    @GetMapping(path = "/refresh")
    public String refresh(@RequestParam Integer year, @RequestParam String city, @RequestParam String state) {
        var url = "https://api.calendario.com.br?ano=%d&cidade=%s&estado=%s&token=bWlkaWFuZXRAZ21haWwuY29tJmhhc2g9MjM5MjIzNjk0";
        var ret = new RestTemplate().getForObject(String.format(url, year, city, state), String.class);
        if (ObjectUtils.isEmpty(ret)) return "No Return events";
        var calendar = new Gson().fromJson(XML.toJSONObject(ret).toString(), Calendar.class);
        if (ObjectUtils.isEmpty(calendar)) return "No Return events";
        repository.deleteAllByCityAndState(city, state);
        calendar.getEvents().getEvent().forEach(event -> {
            var hollyday = Hollyday.builder()
                    .city(calendar.getEvents().getLocation().getCity())
                    .state(calendar.getEvents().getLocation().getState())
                    .country(calendar.getEvents().getLocation().getCountry())
                    .name(event.getName())
                    .date(LocalDate.parse(event.getDate(), DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                    .description(event.getDescription())
                    .link(event.getLink())
                    .type(event.getType())
                    .typeCode(Integer.parseInt(event.getType_code()))
                    .build();
            repository.save(hollyday);
        });
        return String.format("Found %d events", calendar.getEvents().getEvent().size());
    }

    @GetMapping
    public List<Hollyday> getHollyday(@RequestParam String city, @RequestParam String state) {
        return repository.findByDateAndCityAndState(LocalDate.now(), city, state);
    }

}