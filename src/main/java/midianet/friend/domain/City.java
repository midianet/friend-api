package midianet.friend.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class City {

    @Id
    private Long id;

    @NotNull
    @Column(length = 80,nullable = false)
    private String name;

    @NotNull
    @Column(length = 2,nullable = false)
    private String state;

    @NotNull
    @Column(length = 2,nullable = false)
    private String country;

}