package midianet.friend.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Token {
    private static Token instance;
    private int position = 0;
    private List<String> keys    = new ArrayList();
    private Map<String,Key> used = new HashMap();

    public static Token getInstance(){
        if(instance == null) instance = new Token();
        return instance;
    }

    private Token(){
        keys.add("1abe82b87ffabbf0e81a9834678f38d5"); //Marcos
        keys.add("2aad8a8bf61ad8feb55f70d0e92d5448"); //Michele
        keys.add("8ff3a4efdfcfe5ef576555e41d34b221"); //Vinicius Muniz
        keys.add("317607e6a61d4445422fe85a0edfdea6"); //Rodrigo RGV
        keys.add("0712fe0a477269cd7f89697b0a6293d8"); //Bleno
        keys.add("f231ed799df37acc1c7ac2d6cac25b13"); //Leonardo
    }

    public synchronized String getKey(){
        if(keys.size() == 0) return "no Key";
        var key = keys.get(position);
        var full_key = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")).concat(key);
        if(!used.containsKey(full_key)) used.put(full_key,Key.builder().key(key).used(0).build());
        var k = used.get(full_key);
        k.setUsed(k.getUsed() +1);
        if(k.getUsed() >= 299 ) keys.remove(position);
        position++;
        if(position >= keys.size()) position = 0;
        return k.getKey();
    }

}

@Data
@Builder
class Key{
    private String key;
    private long used;
}