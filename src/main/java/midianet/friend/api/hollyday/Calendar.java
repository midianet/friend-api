package midianet.friend.api.hollyday;

import lombok.Data;

@Data
public class Calendar {
    private Events events;
}