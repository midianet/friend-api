package midianet.friend.api.hollyday;

import lombok.Data;

@Data
public class Location {
    private String country;
    private String city;
    private String state;
}