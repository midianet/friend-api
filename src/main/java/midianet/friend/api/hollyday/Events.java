package midianet.friend.api.hollyday;

import lombok.Data;

import java.util.List;

@Data
public class Events {
    private Location location;
    private List<Event> event = null;
}