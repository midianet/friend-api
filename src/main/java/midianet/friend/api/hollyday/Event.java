package midianet.friend.api.hollyday;

import lombok.Data;

@Data
public class Event {
    private String date;
    private String name;
    private String link;
    private String description;
    private String type;
    private String type_code;
}