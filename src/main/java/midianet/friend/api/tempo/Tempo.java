package midianet.friend.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class Tempo {
    private Integer id;
    private String name;
    private String state;
    private String country;
    private Data data;
}
