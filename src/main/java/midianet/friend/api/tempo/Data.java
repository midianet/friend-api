package midianet.friend.api.tempo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
public class Data {
    private String temperature;
    private String windDirection;
    private String windVelocity;
    private String humidity;
    private String condition;
    private String pressure;
    private String icon;
    private String sensation;
    private String date;

}