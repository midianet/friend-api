package midianet.friend.repository;

import midianet.friend.domain.Hollyday;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface HollydayRepository extends JpaRepository<Hollyday, Long> {
    void deleteAllByCityAndState(String city, String state);
    List<Hollyday> findByDateAndCityAndState(LocalDate date, String city, String state);
}