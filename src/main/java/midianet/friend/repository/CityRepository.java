package midianet.friend.repository;

import midianet.friend.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CityRepository extends JpaRepository<City,Long>{
    List<City> findByName(String name);
    List<City> findByNameAndState(String name, String state);
}