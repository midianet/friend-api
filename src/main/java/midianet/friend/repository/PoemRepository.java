package midianet.friend.repository;

import midianet.friend.domain.Poem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PoemRepository extends JpaRepository<Poem,Long>{

}