﻿delete from poem;
ALTER SEQUENCE poem_id_seq RESTART WITH 1;
insert into poem (text)values('Feliz aquele que transfere o que sabe e aprende o que ensina.');

insert into poem (text)values('O que vale na vida não é o ponto de partida e sim a caminhada. Caminhando e semeando, no fim terás o que colher.');

insert into poem (text)values('O saber a gente aprende com os mestres e os livros. A sabedoria se aprende é com a vida e com os humildes.');

insert into poem (text)values('Fiz a escalada da montanha da vida removendo pedras e plantando flores.');

insert into poem (text)values('Todos estamos matriculados na escola da vida, onde o mestre é o tempo.');

insert into poem (text)values('Recria tua vida, sempre, sempre.'||chr(13)||'Remove pedras e planta roseiras e faz doces. Recomeça.');

insert into poem (text)values('Eu sou aquela mulher que fez a escalada da montanha da vida, removendo pedras e plantando flores.');

insert into poem (text)values('Poeta não é somente o que escreve.'||chr(13)||'É aquele que sente a poesia, se extasia sensível ao achado de uma rima à autenticidade de um verso.');

insert into poem (text)values('Mais esperança nos meus passos do que tristeza nos meus ombros.');

insert into poem (text)values('A verdadeira coragem é ir atrás de seu sonho mesmo quando todos dizem que ele é impossível.');

insert into poem (text)values('Não morre aquele que deixou na terra a melodia de seu cântico na música de seus versos.');

insert into poem (text)values('Eu sou aquela mulher a quem o tempo muito ensinou.'||chr(13)||'Ensinou a amar a vida e não desistir da luta,
recomeçar na derrota, renunciar a palavras e pensamentos negativos.'||chr(13)||'Acreditar nos valores humanos e ser otimista.');

insert into poem (text)values('Coração é terra que ninguém vê.');

insert into poem (text)values('Nas palmas de tuas mãos leio as linhas da minha vida.');

insert into poem (text)values('Não lamente o que podia ter e se perdeu por caminhos errados e nunca mais voltou.');

insert into poem (text)values('Me esforço para ser melhor a cada dia.'||chr(13)||'Pois bondade também se aprende.');

insert into poem (text)values('Melhor do que a criatura, fez o criador a criação.'||chr(13)||'A criatura é limitada ao tempo, o espaço, normas e costumes, erros e acertos.'||chr(13)||'A criação é ilimitada, excede o tempo e o meio, projeta-se no Cosmos.');

insert into poem (text)values('“A quem te pedir um peixe, dá uma vara de pescar.”'||chr(13)||'Pensando bem, não só a vara de pescar, também a linhada, o anzol, a chumbada, a isca…');

insert into poem (text)values('Eu sou a dureza desses morros revestidos, enflorados, lascados a machado, lanhados, lacerados.'||chr(13)||'Queimados pelo fogo Pastados Calcinados e renascidos.');


insert into poem (text)values('Bem por isso mesmo diz o caboclo: a alegria vem das tripas — barriga cheia, coração alegre. O que é pura verdade.');

insert into poem (text)values('Minha vida, meus sentimentos, minha estética, todas as vibrações de minha sensibilidade de mulher, têm, aqui, suas raízes.');


insert into poem (text)values('Quando as coisas ficam ruins, é sinal de que as coisas boas estão por perto...');


insert into poem (text)values('Que pretendes, mulher?'||chr(13)||'Independência, igualdade de condições...
Empregos fora do lar?'||chr(13)||'És superior àqueles que procuras imitar.
Tens o dom divino de ser mãe.'||chr(13)||'Em ti está presente a humanidade!');


insert into poem (text)values('Procuro suportar todos os dias minha própria personalidade renovada, despencando dentro de mim tudo que é velho e morto.');


insert into poem (text)values('Eu sou a terra, eu sou a vida.'||chr(13)||'Do meu barro primeiro veio o homem.
De mim veio a mulher e veio o amor.'||chr(13)||'Veio a árvore, veio a fonte.'||chr(13)||'Vem o fruto e vem a flor.');

insert into poem (text)values('Feliz aquele que aprende o que ensina e transfere o que sabe.');


insert into poem (text)values('O amigo não passa a mão'||chr(13)||'Quando fizemos algo errado'||chr(13)||'Está firme ao nosso lado'||chr(13)||'Puxa a orelha, chama a razão!');

insert into poem (text)values('Nunca escreverei uma palavra para lamentar a vida. Meu verso é água corrente, é tronco, é fronde, é folha, é semente, é vida!');


insert into poem (text)values('Melhor professor nem sempre é o de mais saber e, sim, aquele que, modesto, tem a faculdade de manter o respeito e a disciplina da classe.');


insert into poem (text)values('Sei que o mundo não é um mistério e nem um o sonho é uma jura secreta. O Deus que criou o mundo criou também o poeta.');


insert into poem (text)values('Com as pedras que me atiraram construí a minha obra...');


insert into poem (text)values('Criança periférica, rejeitada'||chr(13)||'Teu mundo é o submundo.');


insert into poem (text)values('Acredito nos jovens à procura de caminhos novos, abrindo espaços largos na vida. Creio na superação das incertezas deste fim de século.');


insert into poem (text)values('Tenho consciência de ser autêntica e procuro superar todos os dias minha própria personalidade, despedaçando dentro de mim tudo que é velho e morto, pois lutar é a palavra vibrante que levanta os fracos e determina os fortes... O importante é semear.');

insert into poem (text)values('Aprendi que mais vale lutar do que recolher tudo fácil. Antes acreditar do que duvidar.');


insert into poem (text)values('Ensinou a amar a vida, não desistir de lutar, renascer da derrota, renunciar às palavras e pensamentos negativos.
Acreditar nos valores humanos e a ser otimista.'||chr(13)||'Aprendi que mais vale tentar do que recuar...'||chr(13)||'Antes acreditar que duvidar, o que vale na vida não é o ponto de partida e sim a nossa caminhada.');


insert into poem (text)values('Aprendi que mais vale lutar do que recolher dinheiro fácil. Antes acreditar do que duvidar.');


insert into poem (text)values('Versos... não. Poesia... não. Um modo diferente de contar velhas estórias.');

insert into poem (text)values('Nasci em tempos rudes. Aceitei contradições, lutas e pedras como lições de vida e delas me sirvo. Aprendi a viver.');

insert into poem (text)values('Alguém deve rever, escrever e assinar os autos do Passado antes que o Tempo passe tudo a raso.');


insert into poem (text)values('Feliz aquele que transfere o que sabe e aprende o que ensina.'||chr(13)||'O saber se aprende com mestres e livros.
A Sabedoria, com o corriqueiro, com a vida e com os humildes.'||chr(13)||'O que importa na vida não é o ponto de partida, mas a caminhada.'||chr(13)||'Caminhando e semeando, sempre se terá o que colher.');


insert into poem (text)values('O grande livro que sempre me valeu e que aconselho aos jovens, um dicionário. Ele é o pai, é tio, é avô, é amigo e é um mestre. Ensina, ajuda, corrige, melhora, protege. Dá origem da gramática e o antigo das palavras. A pronúncia correta, a vulgar e a gíria.');


insert into poem (text)values('Sou espiga e o grão que retornam à terra.'||chr(13)||'Minha pena (esferográfica) é a enxada que vai cavando,
é o arado milenário que sulca.'||chr(13)||'Meus versos têm relances de enxada, gume de foice
e o peso do machado.'||chr(13)||'Cheiro de currais e gosto de terra.');


insert into poem (text)values('O que vale na vida não é o Ponto de partida e sim Caminhada.'||chr(13)||'Caminhando e Semeando no fim terás o que colher.');


insert into poem (text)values('Sei que o mundo não e um mistério, e nem o sonho ou uma jura secreta, o Deus que criou o mundo, criou tambem o poeta.');